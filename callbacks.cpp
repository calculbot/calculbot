
/*
 *	PROGRAM:
 *		CalculBot
 *
 *	AUTHOR:
 *		Théophile "Tobast" BASTIAN
 *		Natacha "Io-EE" CHADI
 *
 *	CONTACT & WEBSITE:
 *		Tobast:	http://tobast.fr/
 *		Io-EE:	io-ee[at]gmx[dot]fr
 *
 *	WHAT IS IT?
 *		CalculBot is an IRC bot which sends on a channel the result of an expression given by "!expr"
 *
 *	LICENSE:
 *		Copyright (C) 2011  Théophile BASTIAN and Natacha CHADI
 *
 *		This program is free software: you can redistribute it and/or modify
 *		it under the terms of the GNU General Public License as published by
 *		the Free Software Foundation, either version 3 of the License, or
 *		(at your option) any later version.
 *
 *		This program is distributed in the hope that it will be useful,
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *		GNU General Public License for more details.
 *
 *		You should have received a copy of the GNU General Public License
 *		along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "callbacks.h"

using namespace std;

void connected(irc_session_t* session, const char *event, const char *origin, const char **params, unsigned int count) // called when connected
{
	cout << "Now connected to IRC server. Joining channel..." << endl;

	SettingsRead::ConnectInfo connectInfo=settings.getConnectInfo();

	if(!connectInfo.passwd.empty())
	{
		string identCommand="identify ";
		identCommand+=connectInfo.passwd;
		sendmsg(session, "NickServ", identCommand);
	}

	for(unsigned i=0 ; i<connectInfo.chan.size() ; i++)
	{
		if(irc_cmd_join(session, connectInfo.chan[i].c_str(), NULL) != 0)
		{
			cerr << "*** FATAL ERROR OCCURED WHILE JOINING A CHANNEL ***" << endl << "Error : " << irc_errno(session) << endl;
			throw CMD_ERR;
		}
	}

	cout << "Done." << endl;
	
	for(unsigned i=0;i<connectInfo.chan.size();i++)
		sendmsg(session, connectInfo.chan[i], "Hello world!", true);
}

void call_channel(irc_session_t* session, const char *event, const char *origin, const char **params, unsigned int count)
{
	string outmessage="";
	string message(params[1]);

	if(message.find("!")!=0) // do not process
		return;
	
	message.erase(message.begin(), message.begin()+1);

	outmessage=processMessage(message);

	if(outmessage.empty()) // isn't a valid expression
		return;

	
	string toInsert=origin;
	toInsert.erase(toInsert.begin()+toInsert.find('!'), toInsert.end());
	toInsert+=": ";
	outmessage.insert(0, toInsert);
	sendmsg(session, params[0], outmessage);
}

void osef(irc_session_t* session, const char *event, const char *origin, const char **params, unsigned int count)
{ /* They said we don't need to implement it. I got segfault without. Who's wrong ? */ }

void osef_num(irc_session_t *session, unsigned int event, const char *origin, const char **params, unsigned int count) {} // idem

void osef_dcc_chat(irc_session_t *session, const char *nick, const char *addr, irc_dcc_t dccid) {} // idem

void osef_dcc_send(irc_session_t *session, const char *nick, const char *addr, const char *filename, unsigned long size, irc_dcc_t dccid) {} // idem

