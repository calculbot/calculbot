
/*
 *	PROGRAM:
 *		CalculBot
 *
 *	AUTHOR:
 *		Théophile "Tobast" BASTIAN
 *		Natacha "Io-EE" CHADI
 *
 *	CONTACT & WEBSITE:
 *		Tobast:	http://tobast.fr/
 *		Io-EE:	io-ee[at]gmx[dot]fr
 *
 *	WHAT IS IT?
 *		CalculBot is an IRC bot which sends on a channel the result of an expression given by "!expr"
 *
 *	LICENSE:
 *		Copyright (C) 2011  Théophile BASTIAN and Natacha CHADI
 *
 *		This program is free software: you can redistribute it and/or modify
 *		it under the terms of the GNU General Public License as published by
 *		the Free Software Foundation, either version 3 of the License, or
 *		(at your option) any later version.
 *
 *		This program is distributed in the hope that it will be useful,
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *		GNU General Public License for more details.
 *
 *		You should have received a copy of the GNU General Public License
 *		along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef DEF_FCTCALLBACK
#define DEF_FCTCALLBACK

#include <libircclient/libircclient.h>
#include <iostream>
#include <pthread.h>
#include <string>
#include "data.h"
#include "func.h"
#include "SettingsRead.h"

extern SettingsRead settings;

void connected(irc_session_t* session, const char *event, const char *origin, const char **params, unsigned int count); // called when connected
void call_channel(irc_session_t* session, const char *event, const char *origin, const char **params, unsigned int count);

void osef(irc_session_t* session, const char *event, const char *origin, const char **params, unsigned int count); // see .cpp 
void osef_num(irc_session_t *session, unsigned int event, const char *origin, const char **params, unsigned int count);
void osef_dcc_chat(irc_session_t *session, const char *nick, const char *addr, irc_dcc_t dccid);
void osef_dcc_send(irc_session_t *session, const char *nick, const char *addr, const char *filename, unsigned long size, irc_dcc_t dccid);

#endif //DEF_FCTCALLBACK

