
/*
 *	PROGRAM:
 *		CalculBot
 *
 *	AUTHOR:
 *		Théophile "Tobast" BASTIAN
 *		Natacha "Io-EE" CHADI
 *
 *	CONTACT & WEBSITE:
 *		Tobast:	http://tobast.fr/
 *		Io-EE:	io-ee[at]gmx[dot]fr
 *
 *	WHAT IS IT?
 *		CalculBot is an IRC bot which sends on a channel the result of an expression given by "!expr"
 *
 *	LICENSE:
 *		Copyright (C) 2011  Théophile BASTIAN and Natacha CHADI
 *
 *		This program is free software: you can redistribute it and/or modify
 *		it under the terms of the GNU General Public License as published by
 *		the Free Software Foundation, either version 3 of the License, or
 *		(at your option) any later version.
 *
 *		This program is distributed in the hope that it will be useful,
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *		GNU General Public License for more details.
 *
 *		You should have received a copy of the GNU General Public License
 *		along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "process.h"

using namespace std;

string processExpr(string input)
{
	string output;
	Expr expr;
	
	checkConst(input);
	parseStr(input, expr);
	checkValid(expr);

	checkUselessBraces(expr); // NEVER trust user input.

	while(expr.size() > 1)
	{
		int op=priorityOp(expr);

		switch(expr[op].specVal)
		{
			case ADD:
				expr[op-1].numVal=expr[op-1].numVal+expr[op+1].numVal;
				expr.erase(expr.begin()+op);
				expr.erase(expr.begin()+op);
				break;
			case SUBST:
				expr[op-1].numVal=expr[op-1].numVal-expr[op+1].numVal;
				expr.erase(expr.begin()+op);
				expr.erase(expr.begin()+op);
				break;
			case MULT:
				expr[op-1].numVal=expr[op-1].numVal*expr[op+1].numVal;
				expr.erase(expr.begin()+op);
				expr.erase(expr.begin()+op);
				break;
			case DIV:
				if(expr[op+1].numVal==0)
					throw -1;
				expr[op-1].numVal=expr[op-1].numVal/expr[op+1].numVal;
				expr.erase(expr.begin()+op);
				expr.erase(expr.begin()+op);
				break;
			case POWER:
				expr[op-1].numVal=pow(expr[op-1].numVal,expr[op+1].numVal);
				expr.erase(expr.begin()+op);
				expr.erase(expr.begin()+op);
				break;
			default:
				throw -1;
				break;
		}

		checkUselessBraces(expr);
	}
	
	output=intToStr(expr[0].numVal);
	checkOutputConst(output);

	return output;
}

void checkConst(string& str)
{
	// easter eggs
	while(1) {
		unsigned f=str.find("answer");
		if(f==string::npos)
			break;
		str.erase(str.begin()+f, str.begin()+f+6);
		str.insert(f, "42");
	}

	while(1) {
		unsigned f=str.find("tobast");
		if(f==string::npos)
			break;
		str.erase(str.begin()+f, str.begin()+f+6);
		str.insert(f, "708457");
	}

	while(1) {
		unsigned f=str.find("Io-EE");
		if(f==string::npos)
			break;
		str.erase(str.begin()+f, str.begin()+f+5);
		str.insert(f, "1033");
	}

	while(1) {
		unsigned f=str.find("leet");
		if(f==string::npos)
			break;
		str.erase(str.begin()+f, str.begin()+f+4);
		str.insert(f, "1337");
	}

	// usefull things
	while(1) {
		unsigned f=str.find("limit");
		if(f==string::npos)
			break;
		str.erase(str.begin()+f, str.begin()+f+5);
		str.insert(f, intToStr(pow(2, sizeof(int)-1)));
	}
}

void checkOutputConst(string& str)
{
	if(str=="42")
		str="The answer to Life, Universe and everything else.";
	else if(str=="404")
		str="Cannot find your answer, sorry.";
}

int strToNum(string str)
{
	int num=0;

	for(int i=str.size()-1;i>=0;i--)
	{   
		if(str[i] < '0' || str[i] > '9')
			throw -1; 
		short currnum=str[i]-'0';
		num+=currnum*pow(10,(str.size()-1)-i);
	}   
	return num;
}

string intToStr(int in)
{
	vector<char> vectStr;

	if(in==0)
		return "0";

	while(in!=0)
	{
		int lastNb=in-((int)(in/10))*10;
		vectStr.push_back(lastNb+'0');
		in/=10;
	}

	string out;
	while(vectStr.size()>0)
	{
		out+=vectStr.back();
		vectStr.pop_back();
	}

	return out;
}

bool isNum(char in)
{
	if(in>='0' && in<='9')
		return true;
	return false;
}

Operator getSpecVal(char in)
{
	Operator op;

	switch(in)
	{
		case '+':
			op=ADD;
			break;
		case '-':
			op=SUBST;
			break;
		case '*':
			op=MULT;
			break;
		case '/':
			op=DIV;
			break;
		case '(':
			op=BRACE;
			break;
		case ')':
			op=ENDBRACE;
			break;
		case '^':
			op=POWER;
			break;
		default:
			op=O_NONE;
			break;
	}

	return op;
}

char getOpChar(Operator in)
{
	char ch;

	switch(in)
	{
		case ADD:
			ch='+';
			break;
		case SUBST:
			ch='-';
			break;
		case MULT:
			ch='*';
			break;
		case DIV:
			ch='/';
			break;
		case BRACE:
			ch='(';
			break;
		case ENDBRACE:
			ch=')';
			break;
		case POWER:
			ch='^';
			break;
		default:
			ch='!';
			break;
	}

	return ch;
}


PriorityLevel getPrio(Operator in)
{
	PriorityLevel prio;
	switch(in)
	{
		case ADD:
		case SUBST:
			prio=ADDSUBST;
			break;
		case MULT:
		case DIV:
			prio=MULTDIV;
			break;
		case BRACE:
		case ENDBRACE:
			prio=BRACES;
			break;
		case POWER:
			prio=P_POWER;
			break;
		default:
			prio=NONE;
			break;
	}
	return prio;
}

void parseStr(std::string input, Expr& expr)
{
	string currNum="";
	for(unsigned i=0;i<input.size();i++)
	{
		if(isNum(input[i]))
		{
			currNum+=input[i];
		}
		else
		{
			if(!currNum.empty())
			{
				Elem elem;
				elem.numVal=strToNum(currNum);
				elem.isNum=true;
				elem.prio=NONE;
				currNum="";
				expr.push_back(elem);
			}

			Elem elem;
			Operator currOp=getSpecVal(input[i]);
			elem.specVal=currOp;
			elem.prio=getPrio(currOp);
			if(currOp==O_NONE)
				throw -1;
			elem.isNum=false;
			expr.push_back(elem);
		}
	}
	if(!currNum.empty())
	{
		Elem elem;
		elem.numVal=strToNum(currNum);
		elem.isNum=true;
		elem.prio=NONE;
		expr.push_back(elem);
	}
}

void checkValid(Expr& expr)
{

	// STEP 1 : sequence like num-op-num-op-num
	bool isNum=true;

	for(unsigned i=0;i<expr.size();i++)
	{
		if(isNum && !expr[i].isNum) 
		{
			if(expr[i].specVal==BRACE || expr[i-1].specVal==ENDBRACE)
				continue;
			throw -1;
		}
		if(!isNum && expr[i].isNum)
			throw -1;
		else
			isNum=!isNum;
	}

	if(isNum && expr.back().specVal!=ENDBRACE) // the last one was an operator
		throw -1;
}

int priorityOp(Expr in)
{
	int prio=-1;
	PriorityLevel currPrio=NONE;

	for(unsigned i=0;i<in.size();i++)
	{
		if(in[i].isNum)
			continue;
		if(in[i].prio > currPrio)
		{
			switch(in[i].specVal)
			{
				case BRACE:
					currPrio=BRACES;
					prio=priorityOp(getBraceVect(in, i))+i+1; // +i+1 -> + index of the first thing inside the braces.
					break;
				case POWER:
					if(!in[i+1].isNum && in[i+1].specVal==BRACE)
					{
						currPrio=BRACES;
						prio=priorityOp(getBraceVect(in, i+1))+i+2; // priority at braces after
					}
					else
					{
						prio=i;
						return prio; // cannot reach higher level of priority
					}
					break;
				default:
					currPrio=in[i].prio;
					prio=i;
					break;
			}
		}
	}

	return prio;
}

Expr getBraceVect(Expr in,int index)
{
	int lastIndex=getClosingBrace(in, index);

	Expr out(in.begin()+index+1, in.begin()+lastIndex-1);
	return out;
}

int getClosingBrace(Expr in, int index)
{
	int lastIndex=-1;
	int nbClosingNeed=1;
	for(unsigned i=index+1;i<in.size();i++)
	{
		if(!in[i].isNum && in[i].specVal==ENDBRACE)
		{
			nbClosingNeed--;
			if(nbClosingNeed==0)
			{
				lastIndex=i;
				break;
			}
		}
		if(!in[i].isNum && in[i].specVal==BRACE)
			nbClosingNeed++;
	}

	if(lastIndex==-1) {
		throw -1; // invalid expr
	}

	return lastIndex;
}

void checkUselessBraces(Expr& in)
{
	for(unsigned i=0;i<in.size();i++)
	{
		if(!in[i].isNum && in[i].specVal == BRACE)
		{
			if(in.size()>i+2 && in[i+1].isNum && (!in[i+2].isNum && in[i+2].specVal==ENDBRACE))
			{
				in.erase(in.begin()+i+2);
				in.erase(in.begin()+i);
			}
			else if(in.size()>i+1 && !in[i+1].isNum && in[i+1].specVal==ENDBRACE)
			{
				in.erase(in.begin()+i+1);
				in.erase(in.begin()+i);
			}
		}
	}
}




void dbg_printElems(Expr in)
{
	for(unsigned i=0;i<in.size();i++)
	{
		if(in[i].isNum)
			cout << in[i].numVal;
		else
			cout << getOpChar(in[i].specVal);
		cout << " << ";
	}
	cout << endl;
}

