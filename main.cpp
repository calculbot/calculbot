
/*
 *	PROGRAM:
 *		CalculBot
 *
 *	AUTHOR:
 *		Théophile "Tobast" BASTIAN
 *		Natacha "Io-EE" CHADI
 *
 *	CONTACT & WEBSITE:
 *		Tobast:	http://tobast.fr/
 *		Io-EE:	io-ee[at]gmx[dot]fr
 *
 *	WHAT IS IT?
 *		CalculBot is an IRC bot which sends on a channel the result of an expression given by "!expr"
 *
 *	LICENSE:
 *		Copyright (C) 2011  Théophile BASTIAN and Natacha CHADI
 *
 *		This program is free software: you can redistribute it and/or modify
 *		it under the terms of the GNU General Public License as published by
 *		the Free Software Foundation, either version 3 of the License, or
 *		(at your option) any later version.
 *
 *		This program is distributed in the hope that it will be useful,
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *		GNU General Public License for more details.
 *
 *		You should have received a copy of the GNU General Public License
 *		along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <iostream>
#include <libircclient/libircclient.h>
#include "callbacks.h"
#include "data.h"

using namespace std;

SettingsRead settings; // We don't have any other choice :'(

int main(void)
{
	try {
		// callbacks functions initialisation
		irc_callbacks_t callbacks;
		callbacks.event_connect=connected;
		callbacks.event_nick=osef;
		callbacks.event_quit=osef;
		callbacks.event_join=osef;
		callbacks.event_part=osef;
		callbacks.event_mode=osef;
		callbacks.event_umode=osef;
		callbacks.event_topic=osef;
		callbacks.event_kick=osef;
		callbacks.event_channel=call_channel;
		callbacks.event_privmsg=osef;
		callbacks.event_notice=osef;
		callbacks.event_invite=osef;
		callbacks.event_ctcp_req=osef;
		callbacks.event_ctcp_rep=osef;
		callbacks.event_ctcp_action=osef;
		callbacks.event_unknown=osef;
		callbacks.event_numeric=osef_num;

		// init
		irc_session_t *session = irc_create_session(&callbacks);

		// reading settings file
		SettingsRead::ConnectInfo connectInfo=settings.getConnectInfo();
		SettingsRead::Identity identity=settings.getIdentity();

		// connection
		cout << "Connecting to \"" << connectInfo.server << "\"..." << endl;
		if(irc_connect(session, connectInfo.server.c_str(), connectInfo.port, NULL,
			identity.nick.c_str(), identity.username.c_str(), identity.realname.c_str()) != 0) // error occured
		{
			cerr << "*** FATAL ERROR OCCURED WHILE CONNECTING TO THE IRC SERVER ***" << endl
				 << "Server : " << connectInfo.server << endl
				 << "Error : " << irc_errno(session) << endl;
			throw CONNECT_ERR;
		}

		if(irc_run(session) != 0) // Main loop will be called automatically
		{
			int errno=irc_errno(session);
			if(errno != 0)
			{
				cerr << "*** FATAL ERROR OCCURED WHILE RUNNING MAIN LOOP IN NETWORK THREAD ***" << endl << "Error : " << irc_errno(session) << endl;
				throw RUN_ERR;
			}
		}

		// deleting session and disconnecting
		irc_destroy_session(session);
	}
	catch(const int err) { // Here we only catch the error.
		switch(err) {
			default:
				break;
		}
	}

	return 0;
}

