FLAGS = -W -Wall -O2
LIB = -lircclient
OBJ = main.o callbacks.o func.o SettingsRead.o process.o
TARGET = CalculBot

all: $(OBJ) data.h
	g++ $(LIB) $(OBJ) -o $(TARGET) $(FLAGS)

main.o: main.cpp
	g++ $(LIB) -c main.cpp -o main.o $(FLAGS)

callbacks.o: callbacks.cpp callbacks.h
	g++ $(LIB) -c callbacks.cpp -o callbacks.o $(FLAGS)

func.o: func.cpp func.h
	g++ $(LIB) -c func.cpp -o func.o $(FLAGS)

process.o: process.cpp process.h
	g++ $(LIB) -c process.cpp -o process.o $(FLAGS)

SettingsRead.o: SettingsRead.cpp SettingsRead.h
	g++ $(LIB) -c SettingsRead.cpp -o SettingsRead.o $(FLAGS)

clean:
	rm -Rf *.o && rm -f $(TARGET)

