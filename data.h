/*
 *	PROGRAM:
 *		TimeNotice
 *
 *	AUTHOR:
 *		Théophile "Tobast" BASTIAN
 *
 *	CONTACT & WEBSITE:
 *		http://tobast.fr/
 *
 *	WHAT IS IT?
 *		"TimeNotice" is an IRC bot which sends a predefinite message on a list of predefinites channels, when an "event" occurs.
 *		Thoses events are defined by a precise time.
 *
 *	LICENSE:
 *		Copyright (C) 2011  Théophile BASTIAN
 *
 *		This program is free software: you can redistribute it and/or modify
 *		it under the terms of the GNU General Public License as published by
 *		the Free Software Foundation, either version 3 of the License, or
 *		(at your option) any later version.
 *
 *		This program is distributed in the hope that it will be useful,
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *		GNU General Public License for more details.
 *
 *		You should have received a copy of the GNU General Public License
 *		along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef DEF_DATA
#define DEF_DATA

enum Err {
	NOERR, CONNECT_ERR, RUN_ERR, CMD_ERR, THREAD_ERR, OPENSETTINGS_ERR, READSETTINGS_ERR
};

#endif // DEF_DATA
