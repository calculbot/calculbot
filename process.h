
/*
 *	PROGRAM:
 *		CalculBot
 *
 *	AUTHOR:
 *		Théophile "Tobast" BASTIAN
 *		Natacha "Io-EE" CHADI
 *
 *	CONTACT & WEBSITE:
 *		Tobast:	http://tobast.fr/
 *		Io-EE:	io-ee[at]gmx[dot]fr
 *
 *	WHAT IS IT?
 *		CalculBot is an IRC bot which sends on a channel the result of an expression given by "!expr"
 *
 *	LICENSE:
 *		Copyright (C) 2011  Théophile BASTIAN and Natacha CHADI
 *
 *		This program is free software: you can redistribute it and/or modify
 *		it under the terms of the GNU General Public License as published by
 *		the Free Software Foundation, either version 3 of the License, or
 *		(at your option) any later version.
 *
 *		This program is distributed in the hope that it will be useful,
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *		GNU General Public License for more details.
 *
 *		You should have received a copy of the GNU General Public License
 *		along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <string>
#include <iostream>
#include <cmath>
#include <vector>

enum PriorityLevel {
	NONE, ADDSUBST, MULTDIV, BRACES, P_POWER
};
enum Operator {
	O_NONE, ADD, SUBST, MULT, DIV, BRACE, ENDBRACE, POWER
};

struct Elem {
	int numVal;
	Operator specVal;
	PriorityLevel prio;

	bool isNum;
};

typedef std::vector<Elem> Expr;

std::string processExpr(std::string input);
void checkConst(std::string& str);
void checkOutputConst(std::string& str);
int strToNum(std::string str);
std::string intToStr(int in);
bool isNum(char in);
Operator getSpecVal(char in);
char getOpChar(Operator in);
PriorityLevel getPrio(Operator in);
void parseStr(std::string input, Expr& expr);
void checkValid(Expr& expr);
int priorityOp(Expr expr);
Expr getBraceVect(Expr in, int index);
int getClosingBrace(Expr in, int index);
void checkUselessBraces(Expr& in);

void dbg_printElems(Expr in);

