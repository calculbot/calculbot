
/*
 *	PROGRAM:
 *		CalculBot
 *
 *	AUTHOR:
 *		Théophile "Tobast" BASTIAN
 *		Natacha "Io-EE" CHADI
 *
 *	CONTACT & WEBSITE:
 *		Tobast:	http://tobast.fr/
 *		Io-EE:	io-ee[at]gmx[dot]fr
 *
 *	WHAT IS IT?
 *		CalculBot is an IRC bot which sends on a channel the result of an expression given by "!expr"
 *
 *	LICENSE:
 *		Copyright (C) 2011  Théophile BASTIAN and Natacha CHADI
 *
 *		This program is free software: you can redistribute it and/or modify
 *		it under the terms of the GNU General Public License as published by
 *		the Free Software Foundation, either version 3 of the License, or
 *		(at your option) any later version.
 *
 *		This program is distributed in the hope that it will be useful,
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *		GNU General Public License for more details.
 *
 *		You should have received a copy of the GNU General Public License
 *		along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "SettingsRead.h"

using namespace std;

SettingsRead::SettingsRead(string filepath)
{
	vector<string> paths; // different possible paths, in preferencial order.
	paths.push_back(filepath);
	paths.push_back("/etc/timenotice/"+filepath);


	for(unsigned i=0;i<paths.size();i++)
	{
		file.open(paths[i].c_str(), ios::in);

		if(file.is_open())
			break;
	}
	if(!file.is_open())
	{
		cerr << "*** FATAL ERROR OCCURED WHILE OPENING THE SETTINGS FILE ***" << endl;
		throw OPENSETTINGS_ERR;
	}

	fillData();
}

SettingsRead::ConnectInfo SettingsRead::getConnectInfo() const
{
	return connectInfo;
}

SettingsRead::Identity SettingsRead::getIdentity() const
{
	return identity;
}

void SettingsRead::fillData()
{
	string line;
	Section sectiontype=UNDEF;

	while(getline(file, line))
	{
		if(firstChar(line) == '[')
		{
			if(line=="[connectinfo]")
				sectiontype=CONNECTINFO;
			else if(line=="[identity]")
				sectiontype=IDENTITY;
		}

		else if(firstChar(line) == '#') // comment
			continue;
		else if(line.empty()) // empty line
			continue;

		else if(sectiontype==CONNECTINFO)
		{
			if(line.find("server=") != string::npos)
			{
				paramFromLine(line);
				connectInfo.server=line;
			}
			else if(line.find("port=") != string::npos)
			{
				paramFromLine(line);
				try { connectInfo.port=strToNum(line); }
					catch(int &e) { connectInfo.port=0; }
			}
			else if(line.find("passwd=") != string::npos)
			{
				paramFromLine(line);
				connectInfo.passwd=line;
			}
			else if(line.find("chan=") != string::npos)
			{
				paramFromLine(line);
				connectInfo.chan.push_back(line);
			}
		}

		else if(sectiontype==IDENTITY)
		{
			if(line.find("nick=") != string::npos)
			{
				paramFromLine(line);
				identity.nick=line;
			} 
			else if(line.find("username=") != string::npos)
			{
				paramFromLine(line);
				identity.username=line;
			} 
			else if(line.find("realname=") != string::npos)
			{
				paramFromLine(line);
				identity.realname=line;
			}
		}
	}
}

char SettingsRead::firstChar(string line)
{
	for(unsigned i=0;i<line.size();i++)
	{
		if(line[i] != ' ' && line[i] != '\t' && line[i] != '\n') // pas un espace blanc
			return line[i]; // on retourne (1er char non-espace
	}
	return ' '; // erreur : un espace.
}

void SettingsRead::paramFromLine(string& line)
{
	line.erase(line.begin(), line.begin()+line.find('"')+1);
	line.erase(line.begin()+line.rfind('"'), line.end());
}

SettingsRead::~SettingsRead()
{
	file.close();
}

